#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <pthread.h> 
#include <sys/time.h>


float sum;
int n = 1000;
int thread_count=4;
int flag;

void* Thread_sum(void* rank) {
	long my_rank = (long) rank;
	double factor;
	long long i;
	long long my_n = n/thread_count;
	long long my_first_i = my_n*my_rank;
	long long my_last_i = my_first_i + my_n;
	//double my_sum = 0.0;
	
	if (my_first_i % 2==0)
		factor =1.0;
	else
		factor = -1.0;
	
	for (i = my_first_i; i< my_last_i ; i++, factor = -factor){
		while(flag!=my_rank){} // webea al thread en el bucle
		//printf("(my_rank :%ld)\n",my_rank );
		sum+= factor/(2*i+1);
		flag = (flag+1) % thread_count;
	}

	return NULL;
}

int main(int argc, char* argv[]){

for(int i=0;i<6;i++){	

	flag = 0; sum =0;
	long thread;
	pthread_t* thread_handles;
	clock_t start, finish;
	double cpu_time_used;
	thread_count= thread_count*2;

	thread_handles = malloc (thread_count*sizeof(pthread_t));

	start=clock();
	for( thread=0; thread< thread_count ; thread++)
		pthread_create(&thread_handles[thread],NULL,Thread_sum,(void*)thread);

	for (thread=0; thread<thread_count; thread++)
		pthread_join(thread_handles[thread],NULL);
	finish=clock();

	free(thread_handles);
	cpu_time_used = ( (double)(finish-start))/CLOCKS_PER_SEC;
	printf("pi value : %f with %d threads\n",sum*4.0,thread_count);
	printf("Elapse Time : %e seconds\n",cpu_time_used);
	}


return 0;
}

























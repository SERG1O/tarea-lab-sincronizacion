#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <pthread.h> 
#include<semaphore.h>

int counter;
int thread_count=8;
int j;
sem_t count_sem, barrier_sem;


void* Thread_work(void* thread);


int main(){
counter=0;
long thread; // numeration of threads
pthread_t* thread_handles; // listsa de threads
sem_init(&count_sem, 1, 1); 
sem_init(&barrier_sem, 1, 0); 

thread_handles = malloc (thread_count*sizeof(pthread_t)); //iinit the list

for( thread=0; thread< thread_count ; thread++)
	pthread_create(&thread_handles[thread],NULL,Thread_work,(void*)thread);

for (thread=0; thread<thread_count; thread++)
	pthread_join(thread_handles[thread],NULL);


free(thread_handles);
printf(" %d threads\n",thread_count);

return 0;
}

void* Thread_work(void* thread){
	
	sem_wait(&count_sem);
	if (counter==thread_count-1){
		counter =0;
		sem_post(&count_sem);
		for(int j=0;j<thread_count-1;j++)
			sem_post(&barrier_sem);	
	 }else{
		counter++;
		sem_post(&count_sem);
		sem_wait(&barrier_sem);// block all the threads
		}

}





#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>

#define SHARED 1
void *Producer(); // Make Declaration of Producer
void *Consumer(); // Make Declaration of Consumer
sem_t empty, full,sm; //Declare semaphores to be used
int data; // data variable
pthread_mutex_t mutex;

int main()
{
	pthread_t ptid,ctid2,ctid3, ctid1; // Line 1
	int x=1,y=2,z=3;
	int *a = &x;
	int *b = &y;
	int *c = &z;
	printf("\nMain Started");
	sem_init(&empty, SHARED, 1); // Line 2
	sem_init(&full, SHARED, 0); // Line 3
	sem_init(&sm, SHARED, 1);// Line 4
	
	pthread_create(&ptid,NULL,Producer,NULL); // Line 5
	pthread_create(&ctid1,NULL,Consumer,(void*)a); 
	pthread_create(&ctid2,NULL,Consumer,(void*)b); 
	pthread_create(&ctid3,NULL,Consumer,(void*)c); 
; 
	pthread_join(ptid,NULL); 
	pthread_join(ctid1,NULL); 
	pthread_join(ctid2,NULL); 
	pthread_join(ctid3,NULL); 
	printf("\nMain done\n");
}


void *Producer(){

int produced;
printf("\nProducer created");
printf("\nProducer id is %ld\n",pthread_self()); //print thread id
	for(produced=0;produced<30;produced++)
	{
                 
		data=produced;           

		printf("\nProducer: %d  ",data);
	}
}

void *Consumer(void* num){

int consumed, total=0;
int thread = (int*)num;
printf("Consumer created, Thread num: %d\n",*thread);
printf("Consumer id is %ld\n",pthread_self());
for(consumed=0;consumed<10;consumed++){
		pthread_mutex_lock(&mutex);                    
		total=total+data;     
		printf("Thread: %d , Consumed: %d",*thread,data);        
		pthread_mutex_unlock(&mutex);
	
	}
printf("\nThe total of 100 iterations is %d\n",total);
}
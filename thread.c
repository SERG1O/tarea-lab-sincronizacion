#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>  //Header file for sleep(). man 3 sleep for details. 
#include <pthread.h> 
  
// A normal C function that is executed as a thread  
// when its name is specified in pthread_create() 
int a = 0;
void *myThreadFun(void *var) 
{ 
    int *id = (int *)var;
    //sleep(1);
    static int b=0;
    b++; a++; 
    printf("Thread Id :%d \n",*id);
    printf("Value of the static variable: %d \n",b);
    printf("Value of global variable:  %d \n",a); 
} 
   
int main() 
{ 
    pthread_t th_id;
    
    for( int i=0; i<10;i++){
    	pthread_create(&th_id, NULL, myThreadFun, (void*)&th_id); 
    }
    pthread_exit(NULL);
    //pthread_join(thread_id, NULL); 
    printf("After Thread\n");
    return 0;
}


#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <pthread.h> 

int counter;
int thread_count=8;
pthread_mutex_t barrier_mutex;


void* Thread_work(void* thread);


int main(){
long thread;
pthread_t* thread_handles;
pthread_mutex_init(&barrier_mutex,NULL);
thread_handles = malloc (thread_count*sizeof(pthread_t));



for( thread=0; thread< thread_count ; thread++)
	pthread_create(&thread_handles[thread],NULL,Thread_work,(void*)thread);

for (thread=0; thread<thread_count; thread++)
	pthread_join(thread_handles[thread],NULL);


free(thread_handles);
pthread_mutex_destroy(&barrier_mutex);

printf(" %d threads\n",thread_count);

return 0;
}

void* Thread_work(void* thread){
	pthread_mutex_lock(&barrier_mutex);
	counter++;
	pthread_mutex_unlock(&barrier_mutex);
	while (counter<thread_count);
}

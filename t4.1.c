#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <pthread.h> 
#include <sys/time.h>

float sum;
int n = 1000;
int thread_count=4;
pthread_mutex_t mutex;

void* Thread_sum(void* rank) {
	long my_rank = (long) rank;
	double factor;
	long long i;
	long long my_n = n/thread_count; //  108/4 = 27
	long long my_first_i = my_n*my_rank;  //27*0, 27*1, 27*2
	long long my_last_i = my_first_i + my_n;
	double my_sum = 0.0;
	
	if (my_first_i % 2==0)
		factor =1.0;
	else
		factor = -1.0;
	
	for (i = my_first_i; i< my_last_i ; i++, factor = -factor){
		my_sum+= factor/(2*i+1);
	}
	pthread_mutex_lock(&mutex);
	//printf("(my_rank :%ld)\n",my_rank );
	sum+= my_sum;
	pthread_mutex_unlock(&mutex);
	return NULL;
}

int main(){

for(int i=0;i<6;i++){
	thread_count= thread_count*2;
	sum =0;
	long thread;
	pthread_t* thread_handles;
	clock_t start, finish;
	double cpu_time_used;

	thread_handles = malloc (thread_count*sizeof(pthread_t));
	pthread_mutex_init(&mutex,NULL);


	start=clock();
	for( thread=0; thread< thread_count ; thread++)
		pthread_create(&thread_handles[thread],NULL,Thread_sum,(void*)thread);

	for (thread=0; thread<thread_count; thread++)
		pthread_join(thread_handles[thread],NULL);
	finish=clock();

	free(thread_handles);
	pthread_mutex_destroy(&mutex);

	cpu_time_used = ( (double)(finish-start))/CLOCKS_PER_SEC;
	printf("pi value : %f with %d threads\n",sum*4.0,thread_count);
	printf("Elapse Time : %e seconds\n",cpu_time_used);
}
return 0;
}

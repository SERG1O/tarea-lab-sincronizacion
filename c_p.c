#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>

void *Producer(); 
void *Consumer(); 
sem_t empty, full,sm; 
int data; 
int totalth[5]={0,0,0,0,0};

int main()
{
	pthread_t ptid,ctid2,ctid3, ctid1; 
	int x=1,y=2,z=3;
	sem_init(&empty, 1, 1); 
	sem_init(&full, 1, 0); 
	sem_init(&sm, 1, 1);
	
	pthread_create(&ptid,NULL,Producer,NULL); 
	pthread_create(&ctid1,NULL,Consumer,(void*)&x); 
	pthread_create(&ctid2,NULL,Consumer,(void*)&y); 
	pthread_create(&ctid3,NULL,Consumer,(void*)&z); 

	pthread_join(ptid,NULL); 
	pthread_join(ctid1,NULL); 
	pthread_join(ctid2,NULL); 
	pthread_join(ctid3,NULL); 
}


void *Producer(){

int produced;
printf("\nProducer created");
printf("\nProducer id is %ld\n",pthread_self()); 
	for(produced=0;produced<30;produced++)
	{
		sem_wait(&empty);
  
		sem_wait(&sm);           
                 
		data=produced;           
       
		sem_post(&sm);

		sem_post(&full);
		printf("\nProducer: %d  ",data);
	}
}

void *Consumer(void* rank){

int consumed, total=0;
int *my_rank = (int*) rank;
printf("Consumer created, Thread num: %d\n",*my_rank);
printf("Consumer id is %ld\n",pthread_self());
for(consumed=0;consumed<10;consumed++){
	sem_wait(&full);      
	sem_wait(&sm);                             
		total = total+data;     
		printf("Thread: %d , Consumed: %d",*my_rank,data);             
	sem_post(&sm);
	sem_post(&empty);
	
	}
printf("\nThe total of iterations is %d\n",total);
}
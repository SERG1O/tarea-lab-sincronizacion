#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>  //Header file for sleep(). man 3 sleep for details. 
#include <pthread.h> 
  
pthread_mutex_t locked;
int a;

void *func(){
	pthread_mutex_lock(&locked);
	int i =0;
	a++;
	while(i<6){
		printf("%d\n",a);
		
		i++;
	}
	sleep(1);
 	printf("Finished...\n");
 	pthread_mutex_unlock(&locked);
 }	
int main(){
int error;
pthread_t th1, th2;

if (pthread_mutex_init(&locked,NULL) !=0){
	printf("Mutex creation fail\n");
	exit(1);	
}
a=0;
pthread_create(&th1,NULL,func,NULL);
pthread_create(&th2,NULL,func,NULL);

pthread_join(th1,NULL);
pthread_join(th2,NULL);
pthread_exit(NULL);
return 0;

}
